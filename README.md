You can use [Arkindex](https://arkindex.teklia.com/) to annotate documents from your projects.

![projects](img/corpus_selection.png)

## Uploading local documents to Arkindex

You can upload images to a project on Arkindex by clicking `Import files` in the `Actions` dropdown menu in the upper-right corner.

![actions_import_files](./img/import_files/actions_importfiles.png)

This takes you to the file import interface, in which you must first select the local files from your computer, then upload them to Arkindex, then select a folder and page type (by default, `folder` and `page`) to be created by this import.

![import_local_files](./img/import_files/import_files.png)  

![upload_files](./img/import_files/upload_files.png)  

![start_import](./img/import_files/import.png)  

Once the import is started, you can monitor its progress (but closing this won't stop your import.)

![worklow_monitoring](./img/import_files/workflow_status.png)

When the import is complete, you can click the `View Element` button to take a look a the created folder and pages. You can **change a folder's name** by clicking the pencil-shaped icon next to it.

![edit_folder_name](./img/import_files/edit_name.png)

You can also [upload Transkribus documents to Arkindex](https://teklia.com/arkindex/transkribus/).

## Annotation

The general annotation process is as follows:

- First, you draw a **shape** on the image;
- Then, from that shape you create an **element**;
  - You <ins>have to</ins> select the **type** of the element to create it;
  - You <ins>can</ins> name your element, and assign it a class.
- Once an element is created, you can **edit** it or add a **transcription** to it.

## Annotation interface

The annotation interface can be opened on any page by clicking the icon in the top-right corner.

![open interface](img/open_interface.png)
<br/>
![interface buttons](img/buttons.png)

From top to bottom :

- You can draw *rectangular shapes*,
- Or *polygons*;
- The third button allows you to *select* already existing shapes on the image;
- The \[x\] closes the interface;
- Under the annotation buttons, whether the interface is open or closed, is a *zoom*.

## Element tree

On the left of the image is the **element tree**. This tree allows you to hide, display, and select children elements on your page. The eye icon next to each element is grey when they are hidden, green when they are displayed on the image.

![children tree](img/children_tree.png)

When you hover over an element, two icons appear:

- The first one allows you to edit the element;
- The second one is for adding a transcription to that element.

When you select an element, either directly on the image or from the element tree,  detailed information (such as classes, transcriptions, or creation information) about this element is displayed on the right side of the image.

## Element creation

Once you have drawn a shape on the image, a pop-up window opens for element creation.

![element creation](img/element_creation.png)

When creating an element, you **have to** select its *type*, and you *can* give it a name and assign it a class.
Once an element is created, it shows up on the image and in the element tree.

## Transcription

You can add transcriptions to existing elements by clicking the **\[A+\]** button next to an element in the element tree.  
This opens up a pop-up window into which you can type your text.

![transcribe](img/transcription.png)

## Project specific annotation guides

You can find project-specific annotation guidelines in this repository's folders.  

- [HORAE](HORAE) (medieval illuminated manuscripts)
- [SYNTHESYS+](SYNTHESYS) (herbarium pages, pinned insects, microscope slides)
