## Introduction

A general presentation of Arkindex's annotation interface can be found [here](../README.md).  
For the SYNTHESYS+ project, there are 3 different types of documents to annotate:

- herbarium pages,
- microscope slides,
- pinned insects.

Pinned insects and microscope slides have a somewhat similar structure, with the specimen(s) on one side or the middle of the image, and labels around it, in an orderly fashion. Herbarium pages can be more complex.

## Microscope slides and pinned insects

On these documents, we create elements for the **labels** and for the **text lines**.  
For the labels, we create *field* elements.  
For the text lines, we create *text line* elements.  

The general idea is to **only annotate the elements you are interested in / want the machine learning tool to learn to detect and transcribe**. For example here, there is not need to annotate the QR codes, bar codes or color test cards.

![fields+lines](img/fields_lines.png)  
<br/>
![microscope slide](img/slide_example.png)

When drawing text lines, take care to cut off as little of the text inside as possible, while also ensuring that as few text lines as possible overlap. Overlapping text lines in the annotated data make it harder to get good automatic text line detection results.  
If you want to edit a text line's (or any element's) shape you can do so simply by selecting it, either directly from the image or from the element tree, and then dragging the points to the desired position.

![edit text line](img/edit_element.png)

Then we **transcribe** the text lines.  
To do so, it may be convenient to first filter the elements by type in the *children tree* on the left of the image. Then, when hovering over a text line element, you can click the **\[A+\]** icon, which will open the transcription creation pop-up window.

![filter+transcription](img/filter_addtranscription.png)

You can type the transcribed text into this window, then click create.

![transcription](img/create_transcription.png)

## Herbarium pages

We aren't annotating herbarium pages at this point.
