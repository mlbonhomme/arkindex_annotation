## Introduction

We will be using Arkindex to annotate pages from the HORAE corpus.

The main structural elements that we will be annotating are as follows:
* the **pages**
* the **decorated borders**
* the **miniatures**
* the **text regions**
* the **text lines**
* the **initials**, whether inline or bigger
* and **other decorative elements** that are meaningful in terms of structure, such as line filler decorations.

## How to annotate

You can annotate images by drawing either rectangular shapes or polygons.

![shapes](./img/shapes.png)

Once the shape is drawn, you can assign it an `element type` (required), a `class` and a `name` (optional).

![create element](./img/create_element.png)

**Step 1: the page(s)**

On all images, whether they contain one or two pages, we draw one `Single page` element for each page.

**Step 2: the borders**

To annotate border elements, you can trace either rectangles or polygons. Please note that you cannot draw a polygon with a hole in the middle, so if you want to annotate borders that go around the whole page with a polygon, you can do it like this:

![border polygon](./img/poly_marges.png)

or you can trace multiple shapes.

Borders are annotated as `Decoration` elements. The type of border is then specified by adding a class:
  * `decorated_border` for border decorations that are not meaningful, do not resemble miniatures.
  * `illustrated_border` for miniatures that are part of a border decoration.

**Step 3: the miniatures**

If the page contains an illustration that isn't part of an ornamental border, it is annotated as a `Miniature` element.

**Step 4: the text**

If the page contains text, each text area must first be annotated as a `Text` element. If there are multiple columns of text, each columns is a `Text` element. If there is text in the borders, that text is also annotated as a `Text` element, but with the `border_text` class.

To indicate reading order, you can draw the `Text` elements in the logical reading order, or number them by naming them:

![name text](./img/name_text.png)

**Step 5: the initials**

Illuminated initials are annotated as `Initial` elements. The type of initial is given by adding a class:
  * `decorated_initial`: for initials that are decorated but do not depict something that would make them resemble a miniature ; this is most of the initials we  encounter.
  * `historiated_initial`: for initials that are decorated so that they actually depict something, like a miniature but with a letter in the middle; decorated initials that depict either a scene or a character. What makes them different from the other decorated initials is that they carry a meaning other than their alphabetical letter. They are bigger than most decorated initials, and much less frequent.
  * `simple_initial`: for initials that aren't at all decorated, but are simply (a little or much) bigger and/or of a different color than the rest of the text.

**Step 6: decorative/structural elements**

All other decorative and/or structural elements are annotated as `Decoration` elements, with a class:
  * `line_filler` for inline filler decorations, that are most often, but not always, drawn at the end of a text line.
  * `ornament` for decorations (within the text) which, unlike miniatures, do not depict anything that carries meaning, and are not line filler decorations either (they are bigger). This includes the bits of ornamental musical notation, which show up in at least on of the manuscripts of the HORAE corpus.

**Step 7: text lines**

Text lines are annotated as `Text line` elements.

## Examples

### line filler decorations

![bout-de-ligne](./img/bdl.png)

![bout-de-ligne2](./img/bdl2.png)

### musical notation

![musique](./img/musique.png)

### decorated initials

![l_historiee](./img/lhistoriee.png)
*historiated_initial*

![l_ornee](./img/lornee.png)
*decorated_initial*

![l_couleur](./img/lcouleur.png)
*simple_initial*
