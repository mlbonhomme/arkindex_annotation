## Introduction

We will be using Transkribus to annotate a sample of pages from the HORAE corpus, and will then use these annotated pages to train a model that will automatically detect the relevant structural elements in these documents.  

The main structural elements that we will be annotating are as follows :
* the **pages**
* the **decorated borders**
* the **miniatures**
* the **text regions**
* the **text lines**
* the **initials**, whether inline or bigger
* and **other decorative elements** that are meaningful in terms of structure, such as line filler decorations.

For the time being, we won't be annotating the calendar pages. We aren't taking text color into account either.

## Install Transkribus

https://transkribus.eu/Transkribus/ : you have to create an account (with a valid email adress) before you can download the software. Once Transkribus is installed on your computer, send an email to `bonhomme@teklia.com` to be added to the **annotation HORAE** collection, which you'll be able to access through the **Server > Collections**  menu :  

![collections](../img/collections.png)

You will also be granted acces to the **test HORAE** collection, where you can have a look at some example pages that have already been annotated.

## Update config.properties

In order to make the annotation easier, I updated Transkribus' config.properties file so that all the personnalised structural tags we will use for HORAE, and only those tags, will be shown in the **Metadata > Structural** tab.

**Step 1** : go to your Transkribus-1.x.x folder, find the `config.properties` file, make a copy of it and store it somewhere, so you can go back to the default configuration whenever you want to.  

**Step 2** : download the `config.properties` file from this repository (https://gitlab.com/mlbonhomme/annotation_horae/blob/master/config.properties) and save or paste it into your Transkribus-1.x.x folder, replacing the original file.

**Step 3** : (re)start Transkribus and check that the **Metadata > Structural** tab has been updated to look like this :

![structure tags](../img/structural.png)

You can change the colors by clicking the **Customize** button, and there clicking on the color of each tag.

## Draw zones

To split the images into zones, we use some of the rectangular "regions" that can be drawn in Transkribus.

**NB :** when you're done using any Transkribus tool, hit **escape** to unselect it and go back to your regular pointer.

![regions](../img/regions.png)

One you have selected a region to draw, you just click somewhere in the image where you want to start drawing, and then click again when your region is drawn (not drag and drop while holding.) That region can afterwards be moved or modified, as you would move or modify a selection in a graphics editor.  
As obvious as it may be, I highly recommend fitting the image to the window (in the toolbar, on the right) to do this segmentation.

**Step 1 : the page(s)**  
We start by drawing an **UnkownRegion** for each physical page. Some images are of single pages, others are double-page spreads. We always draw either one or two UnkownRegions.

**Step 2 : the borders**  
Then, if the page contains ornamental borders, we draw as many **Image** regions as we need to for those borders.  
If the borders make up a whole, like in page 2 of the example document in the test-HORAE collection, you only need to draw as many regions as you need to contain the whole of the border decoration (3 or 4 regions, most of the time.)  
However, if like in pages 8 and 9 of the example document the border decoration is made up of **distinct elements**, we draw one **Image** region per such element. This is because we are distinguishing between purely ornamental decorations, and decorations that actually are illustrations, depicting something. *cf* the next section, about **Tagging**.  
If there is text in the borders, we draw a **TextRegion** for that text.

**Step 3 : the miniatures**  
If the page contains an illustration that isn't part of an ornamental border, we draw an **Image** region for that illustration.

**Step 4 : the text**  
If the page contains text, we draw a **TextRegion** (which is the first region that's offered in Transkribus, labelled **TR**). If the text is split into columns, we draw **one TextRegion for each column**.  
If there is text in the margins, we draw text regions for this text.  

Mind the **order** of the Text Regions, which you can check in the Layout tab : it must be the same as the logical reading order of the text. If it is incorrect, you can change any zone's *Reading Order* property in the **Layout** tab, either by drag-and-dropping it in place or by clicking on the number :  

![readingorder](../img/readingorder.png)

**Step 5 : the "separators"**  
Lastly, we draw **Separator** regions for the initials and other decorative elements within the text that have a meaning in terms of structure, such as line fillers or rectangular ornaments at the beginning or end of a text. There can be a lot of these, especially initials and line filler decorations, in a single page.

## Tag zones

When all the zones are drawn, we can tag them throuh the **Metadata > Structural** tab. At the bottom, you can see the regions you've just added to your image :

![layout](../img/layout.png)

You can then select one or more region(s) and tag them :

![tagging](../img/tagging.png)

* the **page** tag is for the UnknownRegions we drew for each page.
* the **miniature** tag is for all illustrations, whether they take up a whole page or appear alongside text, that are not border decoration.
* the **boutdeligne** tag is used for inline filler decorations, that are most often, but not always, drawn at the end of a text line.
* the **ornement** tag is used for decorations (within the text) which, unlike miniatures, do not depict anything that carries meaning, and are not line filler decorations either (they are bigger).
  * the **n_musique** tag is used for bits of ornamental musical notation, which show up in at least on of the manuscripts of the HORAE corpus.
* for the initials, we use three tags :  
  * **l_ornee** : for initials that are decorated but do not depict something that would make them resemble a miniature ; this is most of the initials we will encounter.
  * **l_historiee** : for initials that are decorated so that they actually depict something, like a miniature but with a letter in the middle. We tag as *l_historiee* the decorated initials that depict either **a scene or a character**. What makes them different from the other decorated initials is that they carry *a meaning other than their alphabetical letter*. These are bigger than most decorated initials, and are also much rarer.
  * **l_couleur** : for initials that aren't at all decorated, but are simply (a little or much) bigger and/or of a different color than the rest of the text.
* for the border decorations, we use two tags :
  * **marges_ornement** : for border decorations that are not meaningful, do not resemble miniatures.
  * **marges_miniature** : for miniatures that are part of a border decoration. The distinction between ornamental decorations and those that qualify as miniatures won't always be easy, for examples see pages 8 and 9 of the example document in the **test-HORAE** collection on Transkribus.
* **marges_texte** : for text that is located in the margins.

## Examples

### line filler decorations

![bout-de-ligne](../img/bdl.png)  

![bout-de-ligne2](../img/bdl2.png)

### musical notation

![musique](../img/musique.png)  

### decorated initials

![l_historiee](../img/lhistoriee.png)   
*l_historiee*

![l_ornee](../img/lornee.png)  
*l_ornee*

![l_couleur](../img/lcouleur.png)  
*l_couleur*

## TextLine recognition

### Run the Layout Analysis

To draw the text lines, we use Transkribus' Layout Analysis tool, which you can find in the **Tools** tab. We will run the **Layout Analysis**, but only to find lines, not text regions : make sure to <u>un-tick the *Find Text Regions* box</u>, then hit **Run**.  
The TextLine recognition will run as a "job" in Transkribus, and you will be notified when it is done. To see the lines, you will need to re-load the page when asked if you want to : do not add annotations while the TextLine recognition is running, as they wouldn't show up once the page is reloaded.  

Be careful not to have any region selected when you run the Layout Analysis : if a TextRegion is selected, the TextLine recognition will only run in that TextRegion. You can go to the **Layout** tab and select the *Page* at the very top to be sure.

It is not a problem if initials or other inline decorations such as *bouts-de-ligne* are included, even partially, inside a TextLine. <u>We want to have the same number of TextLines as there are actual lines in our TextRegion</u> : this is why, in correcting the results, we will merge together lines that may have been split around an inline decoration.

![CITlab](../img/linesd.png)

### Correct the results

You may need to correct the results of this Layout Analysis, mainly :

#### Removing parasitic TextLines
Such as elements of the border decorations that were erroneously identified as text. Simply select the line you want to remove, either in the image or in the **Layout** tab, and delete it.

![delete](../img/delete.png)

#### Merging together TextLines that were split
For example when there is an initial in a line, and it results in the line being split around that initial. To merge TextLines together, you only need to select the lines that you want to merge, either directly on the image or in the **Layout** tab, and use the **merge** tool.

![merge1](../img/merge1.png)  
*this line was originally split in two*

![merge2](../img/merge2.png)  
*the merge tool*

#### Drawing lines that were missed
The easiest way to do this is to use the **baseline** tool. Draw a rather simple baseline, with few distinct points : this way, if the line "box" that is drawn from it needs to be corrected (you may need to make it bigger) it will be much easier. The baseline is drawn under the text, as if it were the line that it has been written on. Transkribus will ask if you want to draw a "parent line" as well : say yes.

![baseline](../img/baseline.png)

#### Expanding or shrinking line "boxes"
If you take a look at the "test HORAE" collection, you will notice that the line boxes that Transkribus draws are generally not as high as the actual text, but rather enclose the middle of a text line without the bits that go up or down : we will **not** be correcting that at this point, except for lines drawn manually if they are smaller than the automatic ones.  

However, if the **lenght** of a line is incorrect, you can expand or shrink that line : select the line box, and then drag-and-drop the border you want to modify.  

![draganddrop](../img/linedd.png)  
*expanding or shrinking a text line*  

If you are shrinking a line box, for example because elements of the border were detected as part of the TextLine, you might need to delete some **points** of that line shape. To do so, select the **Remove point from selected shape** tool and click the points you want to delete ; hit **escape** when you're done.  

![1](../img/point1.png)  

![2](../img/point2.png)  

![3](../img/point3.png)  

At times, for short lines especially, it may be less time-consuming to simply delete the TextLine and draw a new one using the **baseline** tool.

Don't forget to **save** when you're done !

![save](../img/save.png)

## What we are not annotating

There are also elements of the images that we will not be annotating at this point.

* The **page numbers** : they are written on some documents, either in ink or, more often, in pencil. They are of no interest to us at this point, so we don't need to draw a TextRegion or text lines for them.  
![page_number](../img/page_nb.png)

* **Initials** that do not qualify as actual *decorated* initials.  
![initials](../img/ini.png)

* **Text in color** ; some of it may resemble decorated initials, but is actually **rubrication** : you have to take a look at the rest of the page, especially at what you are certain are decorated initials, to differentiate them. We are not annotating rubrication at this point : such colored text is to be treated as regular text, part of text regions and text lines.  
![text in color](../img/txt_color.png)

![exemple](../img/exemple.png)  
*in this image, only the two letters highlighted in pink are decorated initials (the first one as a l_ornee, the second one as l_couleur) ; all the other text in color is rubrication.*

* **Border decorations** that aren't quite important enough to be annotated and/or only the continuation of decorated initials. This will necessarily be quite subjective ; don't worry too much about it.

## Conclusion
As you add annotations to a page in Transkribus, it automatically switches from the **New** status to the **In Progress** status. Once a page's segmentation has been reviewed, its status will be updated to **Ground Truth** as it will be ready to be used as training data.
